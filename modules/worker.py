import os
from datetime import datetime
import time

from PyQt5.QtCore import QObject, pyqtSlot, pyqtSignal, QRunnable, QTimer


class ChargeDischargeSignals(QObject):
    initialized = pyqtSignal(str, str)
    finished = pyqtSignal(str)
    update_labels = pyqtSignal(str, float, float, float, float)
    update_plot = pyqtSignal(str)
    clear_arrays = pyqtSignal()
    stop_next_operation = pyqtSignal()
    start_next_operation = pyqtSignal()
    update_current_operation_label = pyqtSignal(int, int)


class ChargeDischarge(QObject):
    """
    QRunnable object that performs charge or discharge loop. It's used for both single operations
    and cycle. All measurement parameters has to be passed through kwargs and keithley object too.

    Kwargs:
        keithley (pyvisa.resources.serial.SerialInstrument): VISA object that's connected with Keithley
        sender (str): name of tab which starts measurement. It can be: charge, discharge, sweep, cycle
        path (str): absolute path to measurement folder where logfile will be placed
        sampleID (str): name of sample
        operation_type (str): type of measurement. Can be: charge, discharge, sweep
        delay (float) = delay between next measurement readings, seconds
        operation_num (int) = Number of operation that's going to be performed

        == single measurement parameters: charge / discharge ==
        current (float): charge / discharge current
        finish_voltage (float): voltage that finishes measurement
        source (str): source can be chosen as 'V' - V-Source, 'I' - I-Source
        compliance (float): limit of current(V-Source) or voltage(I-Source) that will not be exceed
                            during measurement
        saturation (bool): keep battery under finish_voltage value untill current drop till current_drop value
        current_drop (float, None): float if saturation is True, None if False
        ========================================================

        == Cycle parameters ==
        cycles_to_perform (int): how many cycles should be performed on sample
        rest_time (float): wait time before next operation will be initialized

        Rest of parameters are the same as for single measurement. Only thing that is different is that
        you have to provide all information for both charge and discharge. Because of that name of kwargs
        are the same as for single measurement, but _chg is added for charge and _dchg for discharge operation:

        current_chg, current_dchg
        finish_voltage_chg, finish_voltage_dchg
        source_chg, source_dchg
        compliance_chg, compliance_dchg
        saturation_chg, saturation_dchg
        current_drop_chg, current_drop_dchg
        ========================================================

    """
    def __init__(self, *args, **kwargs):
        super(ChargeDischarge, self).__init__()
        self.args = args
        self.kwargs = kwargs
        self.signals = ChargeDischargeSignals()
        self.signals.clear_arrays.connect(self.clear_arrays)
        self.signals.stop_next_operation.connect(self._stop_next_operation_timer)
        self.signals.start_next_operation.connect(self.run)

        self.current_operation = 0

        self.stop_thread_flag = False
        self.x = []
        self.y = []

        self.get_shared_operation_kwargs()

        if self.sender is None:
            return
        elif self.sender != 'cycle':
            self.get_single_operation_kwargs()
        else:
            self.get_cycle_kwargs()

    def get_shared_operation_kwargs(self):
        self.keithley = self.kwargs.pop('keithley', None)
        self.sender = self.kwargs.pop('sender', None)
        self.path = self.kwargs.pop('path', os.path.join(os.getcwd(), 'Measurements'))
        self.sampleID = self.kwargs.pop('sampleID', 'Unnamed Sample')
        self.operation_type = self.kwargs.pop('operation_type', None)
        self.delay = self.kwargs.pop('delay', None)
        self.operation_num = self.kwargs.pop('operation_num', None)

    def get_single_operation_kwargs(self):
        self.current = self.kwargs.pop('current', None)
        self.finish_voltage = self.kwargs.pop('finish_voltage', None)
        self.source = self.kwargs.pop('source', None)
        self.compliance = self.kwargs.pop('compliance', None)
        self.saturation = self.kwargs.pop('saturation', None)
        self.current_drop = self.kwargs.pop('current_drop', None)

    def get_cycle_kwargs(self):
        self.current_chg = self.kwargs.pop('current_chg', None)
        self.current_dchg = self.kwargs.pop('current_dchg', None)
        self.finish_voltage_chg = self.kwargs.pop('finish_voltage_chg', None)
        self.finish_voltage_dchg = self.kwargs.pop('finish_voltage_dchg', None)
        self.source_chg = self.kwargs.pop('source_chg', None)
        self.source_dchg = self.kwargs.pop('source_dchg', None)
        self.compliance_chg = self.kwargs.pop('compliance_chg', None)
        self.compliance_dchg = self.kwargs.pop('compliance_dchg', None)
        self.saturation_chg = self.kwargs.pop('saturation_chg', None)
        self.saturation_dchg = self.kwargs.pop('saturation_dchg', None)
        self.current_drop_chg = self.kwargs.pop('current_drop_chg', 10)
        self.current_drop_dchg = self.kwargs.pop('current_drop_dchg', 10)
        self.cycles_to_perform = self.kwargs.pop('cycles_to_perform', 1)
        self.rest_time = self.kwargs.pop('rest_time', 60)

    @pyqtSlot()
    def run(self):
        """Because this function is ran just once and QTimers have to be
        created after object is moved to thread, here we just initialize
        this timers and proceed to loop initialization
        """
        self.loop_timer = self.create_timer(self._charge_discharge_loop)
        if self.sender == 'cycle':
            self.next_operation_timer = self.create_timer(self._loop_initialization, setSingleShot=True)
        self._loop_initialization()

    def create_timer(self, slot, setSingleShot=False):
        """
        Create timer that lives in moved worker thread
        """
        timer = QTimer()
        timer.setSingleShot(setSingleShot)
        timer.timeout.connect(slot)
        return timer

    @pyqtSlot()
    def _stop_next_operation_timer(self):
        """Function called when START/STOP button is pressed.
        It will stop the timer that counts toward initializing next
        operation and after that send finished signal so QThread
        can be deleted safely
        """
        if self.next_operation_timer.isActive():
            self.next_operation_timer.stop()
            self.signals.finished.emit(self.sender)

    def _loop_initialization(self):
        """
        Function that initializes the main loop. Before loop can start, several things have to be set:
        1. If this is cycling operation, we update operation parameters
        2. Load parameters to Keithley device
        3. Create logfile and write headers
        4. Initialize measurement variables: capacity, time elapsed since start of measurement
        5. Turn the output
        6. Start interval timer that's connected to reading function
        """
        if self.sender == 'cycle':
            self.current = self.current_chg if self.operation_type == 'charge' else self.current_dchg
            self.finish_voltage = self.finish_voltage_chg if self.operation_type == 'charge' else self.finish_voltage_dchg
            self.source = self.source_chg if self.operation_type == 'charge' else self.source_dchg
            self.compliance = self.compliance_chg if self.operation_type == 'charge' else self.compliance_dchg
            self.saturation = self.saturation_chg if self.operation_type == 'charge' else self.saturation_dchg
            self.current_drop = self.current_drop_chg if self.operation_type == 'charge' else self.current_drop_dchg

            self.signals.update_current_operation_label.emit(self.current_operation+1, self.cycles_to_perform*2)

        self.signals.initialized.emit(self.sender, self.source)
        print(f"{self.operation_type} operation initialized.\n"
              f"Sample ID\t{self.sampleID}\n"
              f"Finish\t{self.finish_voltage}\tV\n"
              f"Source\t{self.source}\n"
              f"Current\t{self.current}\tA\n"
              f"Saturation\t{self.saturation}\n"
              f"Current operation\t{self.operation_num}\n"
              )

        self.keithley.load_settings_charge_discharge(self.operation_type, self.source,
                                                     self.current, self.finish_voltage,
                                                     self.compliance)
        self.file = create_file(self.path, self.sampleID,
                                self.operation_num+self.current_operation,
                                self.operation_type)
        write(self.file, "Voltage\tCurrent\tTime\tCapacity\n", 'w')

        self.x = []
        self.y = []

        self.capacity = 0
        self._total_time = 0
        self._step_time = time.time()
        self.keithley.output_on()
        self.loop_timer.start(self.delay * 1000)

    def _charge_discharge_loop(self):
        """
        Main loop where readings are gathered. After each read from device, measurement is saved
        to the file, signals that updates plot and labels on GUI are sent.
        At the end finish condition is checked
        """
        voltage, current = [float(x) for x in self.keithley.query(":READ?").split(',')]
        t = time.time() - self._step_time
        self._step_time = time.time()
        self.capacity += abs(current) * t / 3.6  # current (A), time (s). Capacity in mAh: (current / 1000) * (time*3600)
        self._total_time += t
        write(self.file, f"{voltage}\t{current}\t{self._total_time}\t{self.capacity}\n")
        if self.source == 'I':
            self.y.append(voltage)
        else:
            self.y.append(current)
        self.x.append(self.capacity)
        self.signals.update_labels.emit(self.sender, voltage, current, self._total_time, self.capacity)
        self.signals.update_plot.emit(self.sender)
        if self._can_finish(self.operation_type, voltage, current) or self.stop_thread_flag:
            self.loop_timer.stop()
            self.keithley.output_off()
            if not self._start_next_operation():
                self.signals.finished.emit(self.sender)


    def _start_next_operation(self):
        if self.sender != 'cycle' or self.stop_thread_flag:
            return False

        self.current_operation += 1
        if self.current_operation < self.cycles_to_perform * 2:
            if self.operation_type == 'charge':
                self.operation_type = 'discharge'
            else:
                self.operation_type = 'charge'
            self.next_operation_timer.start(self.rest_time * 1000)  # self.delay is in s, QTimer needs ms
            return True
        else:
            return False

    def _can_finish(self, operation, voltage, current):
        """Function that checks if current measurement can be finished.
        If it's CCCV mode it decline to finish untill current / current_drop condition
        is satisfied. It also changes value of current
        """
        #TODO: move current manipulation to new function with clear behavior
        if operation == 'charge':
            if round(voltage, 2) >= self.finish_voltage:
                if self.saturation:
                    if current <= self.current / self.current_drop:
                        return True             #
                    return False                # can't be prettier?
                return True                     #
        else:
            if voltage <= self.finish_voltage:
                if self.saturation:
                    current = abs(current)
                    dV = voltage - self.finish_voltage
                    if dV > 0.005:
                        new_current = current * 1.03
                    else:
                        new_current = current / 1.05
                    if voltage < self.finish_voltage / 1.3:
                        return True
                    elif new_current > self.current / self.current_drop:
                        self.keithley.write(f':SOUR:CURR:LEV -{new_current}')
                        return False
                    else:
                        return True
                return True

    def clear_arrays(self):
        self.x.clear()
        self.y.clear()
        time.sleep(0.1)


class SweepSignals(QObject):
    initialized = pyqtSignal()
    finished = pyqtSignal(str)
    update_labels = pyqtSignal(float, float)
    update_plot = pyqtSignal(str)
    stop_next_operation = pyqtSignal()
    clear_arrays = pyqtSignal()


class Sweep(QObject):
    # TODO: it would be nice to have hardware sweep and then just fetch the data from buffer..
    def __init__(self, *args, **kwargs):
        super(Sweep, self).__init__()
        self.args = args
        self.kwargs = kwargs
        self.signals = SweepSignals()
        self.signals.clear_arrays.connect(self.clear_arrays)

        self.keithley = self.kwargs.pop('keithley', None)
        self.start_value = self.kwargs.pop('start_value', None)
        self.finish_value = self.kwargs.pop('finish_value', None)
        self.steps = self.kwargs.pop('steps', None)
        self.step_value = self.kwargs.pop('step_value', None)
        self.delay = self.kwargs.pop('delay', 0)
        self.compliance = self.kwargs.pop('compliance', 1.05)
        self.reverse_sweep = self.kwargs.pop('reverse_sweep', False)
        self.source = self.kwargs.pop('source', None)
        self.sender = self.kwargs.pop('sender', None)
        self.path = self.kwargs.pop('path', os.path.join(os.getcwd(), 'Measurements'))
        self.sampleID = self.kwargs.pop('sampleID', 'Unnamed Sample')
        self.operation_num = self.kwargs.pop('operation_num', 1)
        self.consecutive_sweeps = self.kwargs.pop('consecutive_sweeps', False)
        self.num_of_sweeps = self.kwargs.pop('num_of_sweeps', 1)
        self.delay_scans = self.kwargs.pop('delay_scans', 0)

        self.current_operation = 0

        self.stop_thread_flag = False
        self.x = []
        self.y = []

    @pyqtSlot()
    def run(self):
        self.next_operation_timer = QTimer()
        self.next_operation_timer.setSingleShot(True)
        self.next_operation_timer.timeout.connect(self._init_loop)
        self.signals.stop_next_operation.connect(self._stop_next_operation_timer)
        self._init_loop()

    def _init_loop(self):
        if not self.step_value:
            return
        self.keithley.load_settings_sweep(self.source, self.start_value,
                                          self.compliance)
        self.signals.initialized.emit()

        print("Sweep operation initialized.\n"
              f"Sample ID\t{self.sampleID}\n"
              f"Source\t{self.source}\n"
              f"Start\t{self.start_value}\n"
              f"Finish\t{self.finish_value}\n"
              f"Steps\t{self.steps}\n"
              f"Delay\t{self.delay}\ts\n"
              f"Reverse\t{self.reverse_sweep}\n"
              f"Sweeps\t{self.current_operation + 1} / {self.num_of_sweeps}")

        file = create_file(
            self.path,
            self.sampleID,
            self.operation_num+self.current_operation,
            "sweep"
        )
        write(file, "Voltage\tCurrent\tTime\n", 'w')

        self.main_loop(self.start_value, self.finish_value, file)
        if self.reverse_sweep and not self.stop_thread_flag:
            self.main_loop(self.finish_value, self.start_value, file)

        self.current_operation += 1
        if self.consecutive_sweeps and self.current_operation < self.num_of_sweeps:
            self.next_operation_timer.start(self.delay_scans * 1000)
        else:
            self.signals.finished.emit(self.sender)

    def main_loop(self, start_value, finish_value, file):
        current_step = 0
        new_value = start_value
        self.keithley.output_on()
        start_time = time.time()
        while current_step <= self.steps:
            print(new_value)
            if self.stop_thread_flag:
                break
            # write new value to device, check what source is used first so good command
            # is sent. First iteration will always be same value as start_value
            if self.source == 'V':
                src = 'VOLTAGE'
            else:
                src = 'CURRENT'
            self.keithley.write(f':SOURCE:{src}:RANG {new_value}')
            self.keithley.write(f":SOURCE:{src}:LEV {new_value}")
            # get a reading
            voltage, current = [float(x) for x in self.keithley.query(":READ?").split(',')]
            read_time = time.time() - start_time
            # write reading to file
            write(file, f"{voltage}\t{current}\t{read_time}\n")
            # add readings to lists for plotter
            self.x.append(voltage)
            self.y.append(current)
            # emit signals to update GUI labels and update plot
            self.signals.update_labels.emit(voltage, current)
            self.signals.update_plot.emit("sweep")  # arg is sender="sweep"
            # get new value for Keithley device
            if start_value > finish_value:
                new_value -= self.step_value
            else:
                new_value += self.step_value
            # wait for next step
            time.sleep(self.delay)
            current_step += 1
        self.keithley.output_off()

    def _stop_next_operation_timer(self):
        self.next_operation_timer.stop()
        self.signals.finished.emit(self.sender)

    def clear_arrays(self):
        self.x.clear()
        self.y.clear()
        time.sleep(0.1)


def write(file: str, row: str, mode='a'):
    """
    Write string to file.
    :param file: path to the file
    :param row: row to write
    :param mode: Check https://docs.python.org/3/library/functions.html#open

    """
    with open(file, mode) as f:
        f.write(row)


def create_file(path, sampleID, operation_num, operation_type) -> str:
    """
    Create new file (and folders that point to this file)
    :param path:
    :param sampleID:
    :param operation_num:
    :param operation_type:
    :return: full path to file
    """
    path = os.path.join(path, 'raw_data')
    if not os.path.isdir(path):
        os.makedirs(path)

    date = datetime.strftime(datetime.now(), "%Y.%m.%d--%H.%M.%S")
    filename = f'{date}_{sampleID}_{operation_type}_{operation_num}.csv'

    if os.path.isfile(os.path.join(path, filename)):
        _file = filename
        i = 1
        while os.path.isfile(os.path.join(path, _file)):
            _file = filename[:-4] + f"({i})" + filename[-4:]
            i += 1
        filename = _file
    full_dir = os.path.join(path, filename)
    open(full_dir, 'w+').close()
    return full_dir