from PyQt5.QtCore import QObject
import time
import visa
from time import sleep
# from concurrent.futures import ThreadPoolExecutor


class Keithley(QObject):
    """Class that handles communication with Keithley 2400 SM device"""
    def __init__(self, address=None):
        super(Keithley, self).__init__()
        self.rsense = "OFF"
        self.device = self.connect(address)

    def connect(self, adr=None):
        # just for tests:
        if adr == 'mock':
            device = Device()
            print("Dummy device")
            return device

        rm = visa.ResourceManager()
        if adr:
            device = rm.open_resource(adr)
            return device

        for adr in rm.list_resources():
            try:
                device = rm.open_resource(adr)
                time.sleep(0.1)
                ID = device.query("*IDN?")
                if "MODEL 2400" in ID:
                    print(ID)
                    return device
            except visa.VisaIOError:
                continue
            device.close()
        print("Couldn't connect to device")
        return None

    def load_settings_charge_discharge(self, operation, source, current, finish_voltage, voltage_compliance):
        if operation == 'discharge':
            current = -abs(current)
        else:
            current = abs(current)

        self.device.write("*RST")
        self.device.write(":SYSTEM:TIME:RESET")
        self.device.write(":OUTPUT:SMODE HIMP")
        self.device.write(f":SYSTEM:RSENSE {self.rsense}")
        if source == 'V':
            self.device.write(":SOUR:FUNC VOLTAGE")
            self.device.write(":SOUR:VOLTAGE:MODE FIXED")
            self.device.write(f":SOUR:VOLTAGE:RANG {finish_voltage}")
            self.device.write(f":SOUR:VOLTAGE:LEV {finish_voltage}")
            self.device.write(":SENS:FUNC 'CURRENT'")
            self.device.write(f":SENS:CURRENT:PROT {current}")
            self.device.write(":SENS:CURRENT:RANG:AUTO ON")
        elif source == 'I':
            self.device.write(':SOUR:FUNC CURRENT')
            self.device.write(':SOURCE:CURRENT:MODE FIXED')
            self.device.write(f':SOURCE:CURRENT:RANG {current}')
            self.device.write(f':SOURCE:CURRENT:LEV {current}')
            self.device.write(":SENS:FUNC 'VOLT'")
            self.device.write(f':SENS:VOLT:PROT {voltage_compliance}')
            self.device.write(':SENS:VOLT:RANG:AUTO ON')
        self.device.write(':FORM:ELEM VOLT, CURR')  # store only voltage and current value

    def load_settings_sweep(self, source, start_value, compliance):
        if source == 'I':
            src = 'CURRENT'
            sens = 'VOLTAGE'
        else:
            src = 'VOLTAGE'
            sens = 'CURRENT'

        self.device.write("*RST")
        self.device.write(":SYSTEM:TIME:RESET")
        self.device.write(":OUTPUT:SMODE HIMP")
        self.device.write(f":SYSTEM:RSENSE {self.rsense}")
        self.device.write(f":SOUR:FUNC {src}")
        self.device.write(f"SOUR:{src}:MODE FIXED")
        self.device.write(f":SOUR:{src}:RANG {start_value}")
        self.device.write(f":SOUR:{src}:LEV {start_value}")
        self.device.write(f":SENS:FUNC '{sens}'")
        self.device.write(f":SENS:{sens}:PROT {compliance}")
        self.device.write(f":SENS:{sens}:RANG:AUTO ON")
        self.device.write(':FORM:ELEM VOLT,CURR')  # store only voltage and current value

    def output_on(self):
        self.device.write(":OUTP ON")

    def output_off(self):
        self.device.write(":OUTP OFF")

    def query(self, command):
        return self.device.query(command)

    def write(self, command):
        self.device.write(command)

from random import random
class Device:
    """Mock class that replaces VISA object"""
    def write(self, *args):
        return
    def query(self, *args):
        return f"{random()},{random()}"
    def close(self):
        return
    def output_on(self):
        return
    def output_off(self):
        return


