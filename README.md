# keithley2400-gui
GUI for Keithley 2400 Source Meter that allows to perform charge, discharge (and cycling) and sweep operations. This app was created with intention to communicate with Keithley 2400 via RS232.

Requirements:

Python 3.6+

PyQt5

pyqtgraph

PyVISA
