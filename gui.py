import sys
import os.path

from PyQt5.QtWidgets import QMainWindow, QMessageBox, QApplication, QFileDialog
from PyQt5.QtCore import QSettings, QSize, QThread
from PyQt5.QtGui import QIntValidator, QResizeEvent
from PyQt5 import uic

from modules import worker
from modules.k2400 import Keithley

DEFAULT_DIR = os.path.normpath(os.path.expanduser("~/Documents/Measurements/cycle"))
DEFAULT_DIR_SWEEP = os.path.normpath(os.path.expanduser("~/Documents/Measurements/sweep"))


class MainWindow(QMainWindow):
    """
    Main window of app.
    Plot window is embedded into PyQt app.
    Workers are run in different thread by using QObject -> moveToThread
    """
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.settings = self._init_settings()
        # to speed up initialization you can provide address in settings.ini
        adr = self.settings.value('keithley_address')
        self.keithley = Keithley(address=adr)

        self.init_ui()

    def resizeEvent(self, a0: QResizeEvent) -> None:
        """Overrided default resize event that resizes plot widget according
        to the size of main window
        """
        main_window_old_height = a0.oldSize().height()
        main_window_old_width = a0.oldSize().width()
        main_window_new_height = a0.size().height()
        main_window_new_width = a0.size().width()
        # ignore resizing of the widget on app initialization
        if main_window_old_height == -1 or main_window_old_width == -1:
            return

        plot_widget_current_size = self.plotWidget.size()
        # get new width and height of plot widget
        width_diff = main_window_new_width - main_window_old_width
        plot_widget_new_width = plot_widget_current_size.width() + width_diff

        height_diff = main_window_new_height - main_window_old_height
        plot_widget_new_height = plot_widget_current_size.height() + height_diff

        self.plotWidget.resize(plot_widget_new_width, plot_widget_new_height)

    def init_ui(self):
        """
        Initialize all GUI elements, signals and slots
        """
        # prevents failing to load ui file when program started outside main folder
        ui_file = os.path.join(os.path.split(__file__)[0], 'mainwindow.ui')
        uic.loadUi(ui_file, self)
        self._list_lineedit_boxes_chg_dchg_cycle()
        self._init_sense_checkboxes()
        self._init_show_plot_checkboxes()
        self._init_charge_tab()
        self._init_discharge_tab()
        self._cycle_init_tab()
        self._sweep_init_tab()
        self._init_rest()
        self.show()

    ##### operation management
    def _on_start_button_click(self, sender: str):
        """
        Function that handles cases when START/STOP button is pressed in any tab.
        """
        sender_obj = self.sender()
        if sender_obj.text() == 'START':
            # Safety check that everything is filled in GUI before measurement can be started
            if sender == 'discharge':
                if self.rB_dchar_VSource.isChecked() and self.cB_dchar_saturation.isChecked():
                    self.show_warning_popup("Saturation can not be used with V source during discharging")
                    return
            elif sender == 'cycle':
                if self.rB_cycle_dchar_VSource.isChecked() and self.cB_cycle_dchg_saturation.isChecked():
                    self.show_warning_popup("Saturation can not be used with V source during discharging")
                    return
            _is_started = self.start_operation(sender)
            if _is_started:
                sender_obj.setText("STOP")
        else:
            # force worker to stop by setting a flag
            self.worker.stop_thread_flag = True
            # stop timer that counting toward next operation:
            if self.worker.sender == 'cycle' or self.worker.sender == 'sweep':
                self.worker.signals.stop_next_operation.emit()
            sender_obj.setText("START")

    def _get_operation_params_chg_dchg(self, sender):
        """All parameteres needed for charge/discharge are gathered there.
        It is all information that you can get from fields in charge and discharge tab.
        Parameters are gathered in dictionary which is then passed to worker object,
        then unpacked.
        Returns:
            params :dict: dict with all arguments needed to start measurement
        """
        if sender == 'charge':
            idx = 0
            operation_type = 'charge'
        else:
            idx = 1
            operation_type = 'discharge'
        current = float(self.current[idx].text())
        finish_voltage = float(self.finish_voltage[idx].text())
        source = 'I' if self.source[idx].isChecked() else 'V'
        compliance = float(self.compliance[idx].text()) if source == 'I' else 'default'
        saturation = self.saturation[idx].isChecked()
        current_drop = int(self.current_drop[idx].text()) if saturation else None
        sampleID = self.sampleID[idx].text() if self.sampleID[idx].text() else 'Unnamed Sample'
        operation_num = int(self.operation_num[idx].text())
        path = os.path.join(self.path[idx].toPlainText(), sampleID)
        delay = float(self.delay[idx].text())
        params = dict(sender=sender, operation_type=operation_type, current=current, finish_voltage=finish_voltage,
                      source=source, compliance=compliance, saturation=saturation, current_drop=current_drop,
                      sampleID=sampleID, operation_num=operation_num, path=path, delay=delay,
                      keithley=self.keithley)
        return params

    def _get_operation_params_cycle(self):
        """All parameters needed for cycle are gathered there. It is all information that
        you can get from fields in cycle tab. Parameters are gathered in dictionary which
        is then passed to worker object, then unpacked.
        Returns:
            params :dict: dict with all arguments needed to start measurement
        """
        sender = "cycle"
        operation_type = 'charge' if self.cB_cycle_charge_first.isChecked() else 'discharge'
        # idx = 2 is for charge fields, idx = 3 is for discharge fields
        # fields that are shared between charge and discharge have idx = 2
        current_chg = float(self.current[2].text())
        current_dchg = float(self.current[3].text())
        finish_voltage_chg = float(self.finish_voltage[2].text())
        finish_voltage_dchg = float(self.finish_voltage[3].text())
        source_chg = 'I' if self.source[2].isChecked() else 'V'
        source_dchg = 'I' if self.source[3].isChecked() else 'V'
        compliance_chg = float(self.compliance[2].text()) if source_chg == 'I' else None
        compliance_dchg = float(self.compliance[3].text()) if source_dchg == 'I' else None
        saturation_chg = self.saturation[2].isChecked()
        saturation_dchg = self.saturation[3].isChecked()
        current_drop_chg = int(self.current_drop[2].text()) if saturation_chg else None
        current_drop_dchg = int(self.current_drop[3].text()) if saturation_dchg else None
        operation_num = int(self.operation_num[2].text())
        sampleID = self.sampleID[2].text() if self.sampleID[2].text() else 'Unnamed Sample'
        path = os.path.join(self.path[2].toPlainText(), sampleID)
        delay = float(self.delay[2].text())
        cycles_to_perform = int(self.lE_cycle_run_count.text())
        rest_time = float(self.lE_cycle_rest_time.text())

        params = dict(sender=sender, operation_type=operation_type,
                      current_chg=current_chg, current_dchg=current_dchg,
                      finish_voltage_chg=finish_voltage_chg, finish_voltage_dchg=finish_voltage_dchg,
                      source_chg=source_chg, source_dchg=source_dchg,
                      compliance_chg=compliance_chg, compliance_dchg=compliance_dchg,
                      saturation_chg=saturation_chg, saturation_dchg=saturation_dchg,
                      current_drop_chg=current_drop_chg, current_drop_dchg=current_drop_dchg,
                      sampleID=sampleID, operation_num=operation_num, cycles_to_perform=cycles_to_perform,
                      path=path, delay=delay, rest_time=rest_time, keithley=self.keithley)

        return params

    def _get_operation_params_sweep(self):
        """All parameters needed for sweep are gathered there. It is all information that
        you can get from fields in sweep tab. Parameters are gathered in dictionary which
        is then passed to worker object, then unpacked.
        Returns:
            params :dict: dict with all arguments needed to start measurement
        """
        sender = "sweep"
        source = 'V' if self.rB_sweep_source_V.isChecked() else 'I'
        start_value = float(self.lE_sweep_startValue.text())
        finish_value = float(self.lE_sweep_finishValue.text())
        steps = int(self.lE_sweep_steps.text())
        step_value = self._sweep_get_step_value()
        delay = float(self.lE_sweep_delay.text())
        compliance = float(self.lE_sweep_complianceValue.text())
        reverse_sweep = self.cB_sweep_reverse.isChecked()
        sampleID = self.lE_sweep_sampleID.text() if self.lE_sweep_sampleID.text() else 'Unnamed Sample'
        path = os.path.join(self.tB_sweep_directory.toPlainText(), sampleID)
        operation_num = int(self.lE_sweep_operation_num.text())
        params = dict(start_value=start_value, finish_value=finish_value, steps=steps, step_value=step_value,
                      delay=delay, compliance=compliance, reverse_sweep=reverse_sweep, path=path, sampleID=sampleID,
                      operation_num=operation_num, source=source, sender=sender, keithley=self.keithley)
        if self.groupBox_consecutive_sweeps.isChecked():
            params['consecutive_sweeps'] = True
            params['delay_scans'] = float(self.lE_sweep_delay_scans.text())
            params['num_of_sweeps'] = int(self.lE_sweep_num_of_sweeps.text())
        return params

    def start_operation(self, sender: str):
        """
        Initializing new operation.
        1. Function gather measurement parameters from proper tab
        2. Create new QThread and worker object
        3. Move worker object to QThread and start it
        Return: True if thread started succesfully, False if otherwise
        """
        try:
            # initialization of worker object and sending it to new thread:
            self.thread = QThread()
            if sender != "sweep":
                if sender == "cycle":
                    params = self._get_operation_params_cycle()
                else:
                    params = self._get_operation_params_chg_dchg(sender)
                self.worker = worker.ChargeDischarge(**params)
                self.worker.signals.initialized.connect(self._init_plot_window_chg_dchg_cycle)
                self.worker.signals.update_labels.connect(self._charge_discharge_update_measurement_labels)
                self.worker.signals.update_current_operation_label.connect(self._cycle_update_label_num_of_operation)
            else:
                params = self._get_operation_params_sweep()
                self.worker = worker.Sweep(**params)
                self.worker.signals.initialized.connect(self._init_plot_window_sweep)
                self.worker.signals.update_labels.connect(self._sweep_update_measurement_label)
            self.worker.moveToThread(self.thread)
            self.thread.started.connect(self.worker.run)
            self.worker.signals.update_plot.connect(self.plot_on_data_arrive)
            self.worker.signals.finished.connect(self._on_operation_finish)
            # delete QThread and it's worker when it finishes work
            self.worker.signals.finished.connect(self.thread.quit)
            self.worker.signals.finished.connect(self.worker.deleteLater)
            self.thread.finished.connect(self.thread.deleteLater)
            self.thread.start()
            return True
        except Exception as e:
            print("Couldn't start new thread because of ", e)
            return False

    ##### Charge / discharge tabs
    def _charge_discharge_update_measurement_labels(self, sender: str, voltage: float, current: float, time: float, capacity: float):
        """
        Function that updates labels in GUI after read had been taken from device. Charge, discharge and sweep tab
        are gathered together since all of these use the same values.
        """
        if sender == 'cycle':
            self.label_cycle_voltageValue.setText(str(voltage))
            self.label_cycle_currentValue.setText(str(current))
            self.label_cycle_timeValue.setText(str(time))
            self.label_cycle_capacityValue.setText(str(capacity))
        elif sender == 'charge':
            self.label_char_voltageValue.setText(str(voltage))
            self.label_char_currentValue.setText(str(current))
            self.label_char_timeValue.setText(str(time))
            self.label_char_capacityValue.setText(str(capacity))
        elif sender == 'discharge':
            self.label_dchar_voltageValue.setText(str(voltage))
            self.label_dchar_currentValue.setText(str(current))
            self.label_dchar_timeValue.setText(str(time))
            self.label_dchar_capacityValue.setText(str(capacity))

    def _init_charge_tab(self):
        self.pB_char_START.clicked.connect(lambda: self._on_start_button_click('charge'))
        self.pB_char_browse_directory.clicked.connect(self._on_browse_button_click)

        self.lE_charge_operation_num.setValidator(QIntValidator(self.lE_charge_operation_num))
        # Charge tab
        self.settings.beginGroup('charge')
        self.lE_char_current.setText(self.settings.value('current', 10E-6, type=str))
        self.lE_char_finishVoltage.setText(self.settings.value('finish_voltage', 4.2, type=str))
        self.lE_char_delay.setText(self.settings.value('delay', 1, type=str))
        self.cB_char_saturation.setChecked(self.settings.value('saturation', False, type=bool))
        self.lE_char_saturation_coeff.setText(self.settings.value('saturation_coeff', 10, type=str))
        self.lE_charge_operation_num.setText(self.settings.value('operation_num', 1, type=str))
        if self.settings.value('source', 'I', type=str) == 'I':
            self.rB_char_ISource.setChecked(True)
        else:
            self.rB_char_VSource.setChecked(True)
        path = self.settings.value('directory', DEFAULT_DIR, type=str)
        if path == 'default' or path == DEFAULT_DIR:
            self.tB_char_directory.setText(DEFAULT_DIR)
        else:
            self.tB_char_directory.setText(os.path.normpath(path))
        self.settings.endGroup()

    def _init_discharge_tab(self):
        self.pB_dchar_START.clicked.connect(lambda: self._on_start_button_click('discharge'))
        self.pB_dchar_browse_directory.clicked.connect(self._on_browse_button_click)

        self.lE_discharge_operation_num.setValidator(QIntValidator(self.lE_discharge_operation_num))
        # Discharge tab
        self.settings.beginGroup('discharge')
        self.lE_dchar_current.setText(self.settings.value('current', 10E-6, type=str))
        self.lE_dchar_finishVoltage.setText(self.settings.value('finish_voltage', 3.0, type=str))
        self.lE_dchar_delay.setText(self.settings.value('delay', 1, type=str))
        self.cB_dchar_saturation.setChecked(self.settings.value('saturation', False, type=bool))
        self.lE_dchar_saturation_coeff.setText(self.settings.value('saturation_coeff', 10, type=str))
        self.lE_discharge_operation_num.setText(self.settings.value('operation_num', 1, type=str))
        if self.settings.value('source', 'I', type=str) == 'I':
            self.rB_dchar_ISource.setChecked(True)
        else:
            self.rB_dchar_VSource.setChecked(True)
        self.lE_dchar_voltageCompliance.setText(self.settings.value('voltage_compliance', 4.5, type=str))
        path = self.settings.value('directory', DEFAULT_DIR, type=str)
        if path == 'default' or path == DEFAULT_DIR:
            self.tB_dchar_directory.setText(DEFAULT_DIR)
        else:
            self.tB_dchar_directory.setText(os.path.normpath(path))
        self.settings.endGroup()

    ##### Sweep tab tabs
    def _sweep_init_tab(self):
        self.pB_sweep_START.clicked.connect(lambda: self._on_start_button_click('sweep'))
        self.pB_sweep_browse_directory.clicked.connect(self._on_browse_button_click)

        self.lE_sweep_operation_num.setValidator(QIntValidator(self.lE_sweep_operation_num))
        self.lE_sweep_delay_scans.setValidator(QIntValidator(self.lE_sweep_delay_scans))
        self.lE_sweep_num_of_sweeps.setValidator(QIntValidator(self.lE_sweep_num_of_sweeps))
        # Sweep tab
        self.lE_sweep_startValue.editingFinished.connect(self._sweep_get_step_value)
        self.lE_sweep_finishValue.editingFinished.connect(self._sweep_get_step_value)
        self.lE_sweep_steps.editingFinished.connect(self._sweep_get_step_value)
        self.settings.beginGroup('sweep')
        self.lE_sweep_startValue.setText(self.settings.value('start', -4.2, type=str))
        self.lE_sweep_finishValue.setText(self.settings.value('finish', 4.2, type=str))
        self.lE_sweep_steps.setText(self.settings.value('steps', 100, type=str))
        self.lE_sweep_delay.setText(self.settings.value('delay', 0, type=str))
        self.lE_sweep_operation_num.setText(self.settings.value('operation_num', 1, type=str))
        self.lE_sweep_complianceValue.setText(self.settings.value('compliance', 1.05, type=str))
        self.lE_sweep_delay_scans.setText(self.settings.value('delay_scans', 0.0, type=str))
        self.lE_sweep_num_of_sweeps.setText(self.settings.value('num_of_sweeps', 50, type=str))
        self.rB_sweep_source_V.toggled.connect(self._sweep_update_on_source_change_labels)
        if self.settings.value('source', 'V', type=str) == 'V':
            self.rB_sweep_source_V.setChecked(True)
        else:
            self.rB_sweep_source_I.setChecked(True)
        path = self.settings.value('directory', DEFAULT_DIR_SWEEP, type=str)
        if path == 'default' or path == DEFAULT_DIR_SWEEP:
            self.tB_sweep_directory.setText(DEFAULT_DIR_SWEEP)
        else:
            self.tB_sweep_directory.setText(os.path.normpath(path))
        # self._update_sweep_tab(self.rB_sweep_source_V.isChecked())
        self._sweep_get_step_value()
        self.settings.endGroup()

    def _sweep_update_measurement_label(self, voltage: float, current: float):
        """
        Function that updates labels in GUI after read had been taken from device. Sweep tab.
        """
        self.label_sweep_voltageValue.setText(str(voltage))
        self.label_sweep_currentValue.setText(str(current))

    def _sweep_update_on_source_change_labels(self, state: bool):
        """
        This function is connected to "toggled" signal of V-Source radio button. If this radio button is checked,
        set labels to point to start-stop value as VOLTAGE. If it is unchecked, then only other option is that I-Source
        has been checked - then labels are updated to show start-stop values as CURRENT
        """
        if state:  # V source is selected, otherwise I source is selected
            self.label_sweep_startValue.setText("Start voltage [V]: ")
            self.label_sweep_finishValue.setText("Stop voltage [V]: ")
            self.label_sweep_steps.setText("Steps: ")
            self.label_compliance.setText("Current compliance [A]: ")
        else:
            self.label_sweep_startValue.setText("Start Current [A]: ")
            self.label_sweep_finishValue.setText("Stop Current [A]: ")
            self.label_sweep_steps.setText("Steps: ")
            self.label_compliance.setText("Voltage compliance [V]: ")
        self._sweep_get_step_value()

    def _sweep_get_step_value(self):
        """This function calculates what is the step value which is going to
        be written to Keithley. It works when start voltage, finish voltage
        and steps to perform are provided. It updates label in sweep tab and
        also returns calculated value that is then passed as parameter to
        worker object
        :return: (float) step value
        """
        # gather all required data
        start_val = float(self.lE_sweep_startValue.text().replace(',', '.'))
        finish_val = float(self.lE_sweep_finishValue.text().replace(',', '.'))
        steps = int(self.lE_sweep_steps.text())
        # calculate the step value
        value = abs(start_val - finish_val) / steps
        # do some unit conversion to show more readable label
        if abs(start_val) > 1E-2:
             label_value = round(value * 1000, 3)
             unit = 'mV' if self.rB_sweep_source_V.isChecked() else 'mA'
        else:
            label_value = round(value * 1000000, 3)
            unit = 'uV' if self.rB_sweep_source_V.isChecked() else 'uA'
        # update label
        self.label_sweep_steps.setText(f'Steps (\u0394 = {label_value} {unit}):')
        return value

    ##### Cycle tab tabs
    def _cycle_init_tab(self):
        self.pB_cycle_START.clicked.connect(lambda: self._on_start_button_click('cycle'))
        self.pB_cycle_browse_directory.clicked.connect(self._on_browse_button_click)
        self.rB_cycle_char_ISource.toggled.connect(self._cycle_enable_compliance_chg)
        self.rB_cycle_dchar_ISource.toggled.connect(self._cycle_enable_compliance_dchg)

        self.lE_cycle_operation_num.setValidator(QIntValidator(self.lE_cycle_operation_num))
        # Cycle tab
        self.cB_cycle_chg_saturation.toggled.connect(self._cycle_on_saturation_cb_state_change)
        self.cB_cycle_dchg_saturation.toggled.connect(self._cycle_on_saturation_cb_state_change)
        self.settings.beginGroup('cycle')
        self.lE_cycle_char_current.setText(self.settings.value('charge_current', 10E-6, type=str))
        self.lE_cycle_char_finishVoltage.setText(self.settings.value('charge_finish_voltage', 4.2, type=str))
        if self.settings.value('charge_source', 'I', type=str) == 'I':
            self.rB_cycle_char_ISource.setChecked(True)
        else:
            self.rB_cycle_char_VSource.setChecked(True)
        self.cB_cycle_chg_saturation.setChecked(self.settings.value('charge_saturation', True, type=bool))
        self.lE_cycle_dchar_current.setText(self.settings.value('discharge_current', 10E-6, type=str))
        self.lE_cycle_dchar_finish_volt.setText(self.settings.value('discharge_finish_voltage', 3.0, type=str))
        if self.settings.value('discharge_source', 'I', type=str) == 'I':
            self.rB_cycle_dchar_ISource.setChecked(True)
        else:
            self.rB_cycle_dchar_VSource.setChecked(True)
        self.cB_cycle_dchg_saturation.setChecked(self.settings.value('discharge_saturation', False, type=bool))
        self.lE_cycle_operation_num.setText(self.settings.value('operation_num', 1, type=str))
        self.lE_cycle_delay.setText(self.settings.value('delay', 1, type=str))
        self.lE_cycle_run_count.setText(self.settings.value('run_count', 1, type=str))
        self.lE_cycle_rest_time.setText(self.settings.value('rest_time', 60, type=str))
        path = self.settings.value('directory', DEFAULT_DIR, type=str)
        if path == 'default' or path == DEFAULT_DIR:
            self.tB_cycle_directory.setText(DEFAULT_DIR)
        else:
            self.tB_cycle_directory.setText(os.path.normpath(path))
        self.settings.endGroup()

    def _cycle_on_saturation_cb_state_change(self, state):
        sender = self.sender()
        if not state:
            if sender.objectName() == 'cB_cycle_chg_saturation':
                self.label_cycle_saturation_chg.hide()
                self.lE_cycle_saturation_coeff_chg.hide()
            elif sender.objectName() == 'cB_cycle_dchg_saturation':
                self.label_cycle_saturation_dchg.hide()
                self.lE_cycle_saturation_coeff_dchg.hide()
        else:
            if sender.objectName() == 'cB_cycle_chg_saturation':
                self.label_cycle_saturation_chg.show()
                self.lE_cycle_saturation_coeff_chg.show()
            elif sender.objectName() == 'cB_cycle_dchg_saturation':
                self.label_cycle_saturation_dchg.show()
                self.lE_cycle_saturation_coeff_dchg.show()

    def _cycle_enable_compliance_chg(self, state):
        if state:
            self.label_compliance_chg.setEnabled(state)
            self.lE_cycle_charge_voltageCompliance.setEnabled(state)
        else:
            self.label_compliance_chg.setEnabled(state)
            self.lE_cycle_charge_voltageCompliance.setEnabled(state)

    def _cycle_enable_compliance_dchg(self, state):
        if state:
            self.label_compliance_dchg.setEnabled(state)
            self.lE_cycle_discharge_voltageCompliance.setEnabled(state)
        else:
            self.label_compliance_dchg.setEnabled(state)
            self.lE_cycle_discharge_voltageCompliance.setEnabled(state)

    def _cycle_update_label_num_of_operation(self, current_operation: int, total_operations: int):
        """
        Update operations performed label as cycling continue
        """
        self.label_cycle_op_number.setText(f"{current_operation} / {total_operations}")

    ##### Stuff that affects whole GUI and not single tab
    def _init_sense_checkboxes(self):
        self.sense_cb = [
            self.checkBox_sense_mode_2, self.checkBox_sense_mode_3,
            self.checkBox_sense_mode_4, self.checkBox_sense_mode
        ]

        sense_mode = self.settings.value('general/4wire', False, type=bool)
        for checkbox in self.sense_cb:
            checkbox.clicked.connect(lambda _, cb=checkbox: self.set_sense(cb.isChecked()))

        self.set_sense(sense_mode)

    def _init_show_plot_checkboxes(self):
        """
        Initialized checkboxes that show or hide PlotWidget. Also show or hide widget and resize main window depends on
        default value from settings.ini file
        """
        show_plot = self.settings.value('general/show_plot', False, type=bool)
        self.plot_cb = [self.cB_charge_plot, self.cB_discharge_plot, self.cB_sweep_plot, self.cB_cycle_plot]

        for checkbox in self.plot_cb:
            checkbox.clicked.connect(lambda _, cb=checkbox: self.on_plot_cb_check(cb.isChecked()))

        self.on_plot_cb_check(show_plot)

    def _list_lineedit_boxes_chg_dchg_cycle(self):
        """
        These line edit boxes are used to input measurement parameters. Boxes that store value for the same parameter
        across Charge / Discharge / Sweep tabs are put into lists, where
        list idx = 0    charge
        list idx = 1    discharge
        list idx = 2    cycle charge
        list idx = 3    cycle discharge
        For fields in cycle tab that store shared value for measurements (as delay, sampleID, path to files,
            operation number and saturation coefficient
        """
        self.current = [self.lE_char_current, self.lE_dchar_current,
                        self.lE_cycle_char_current, self.lE_cycle_dchar_current]

        self.finish_voltage = [self.lE_char_finishVoltage, self.lE_dchar_finishVoltage,
                               self.lE_cycle_char_finishVoltage, self.lE_cycle_dchar_finish_volt]

        self.source = [self.rB_char_ISource, self.rB_dchar_ISource,
                       self.rB_cycle_char_ISource, self.rB_cycle_dchar_ISource]

        self.saturation = [self.cB_char_saturation, self.cB_dchar_saturation,
                           self.cB_cycle_chg_saturation, self.cB_cycle_dchg_saturation]

        self.compliance = [self.lE_char_voltageCompliance, self.lE_dchar_voltageCompliance,
                           self.lE_cycle_charge_voltageCompliance, self.lE_cycle_discharge_voltageCompliance]

        self.current_drop = [self.lE_char_saturation_coeff, self.lE_dchar_saturation_coeff,
                             self.lE_cycle_saturation_coeff_chg, self.lE_cycle_saturation_coeff_dchg]

        self.sampleID = [self.lE_char_sampleID, self.lE_dchar_sampleID, self.lE_cycle_sampleID]

        self.path = [self.tB_char_directory, self.tB_dchar_directory, self.tB_cycle_directory]

        self.delay = [self.lE_char_delay, self.lE_dchar_delay, self.lE_cycle_delay]

        self.operation_num = [self.lE_charge_operation_num, self.lE_discharge_operation_num, self.lE_cycle_operation_num]

    def _init_rest(self):
        """
        Here initialization of miscellaneous stuff is done for all tabs:
        """
        # populate values in tabs
        self.settings.beginGroup('general')
        sense_mode = self.settings.value('4wire', False, type=bool)
        show_plot = self.settings.value('show_plot', True, type=bool)
        self.settings.endGroup()

        self.set_sense(sense_mode)
        self.on_plot_cb_check(show_plot)


    def _on_operation_finish(self, sender: str):
        """
        Perform this function when operation for charge/discharge is finished. From GUI side there's really not much
        to do than set text push button from proper tab to "START". Everything related to measurement is done inside
        worker thread when it finishes.
        """
        if sender == "charge":
            self.pB_char_START.setText("START")
        elif sender == "discharge":
            self.pB_dchar_START.setText("START")
        elif sender == "sweep":
            self.pB_sweep_START.setText("START")
        else:  # sender == cycle
            self.pB_cycle_START.setText("START")

    def _on_browse_button_click(self):
        """
        Function called when BROWSE button is clicked. It allows to change path for measurements storage
        """
        sender = self.sender()
        dialog = QFileDialog(self, "Sample Folder")
        path = os.path.normpath(dialog.getExistingDirectory(self, "Select Measurement directory"))
        if sender.objectName() == "pB_char_browse_directory":
            self.tB_char_directory.setText(path)
        elif sender.objectName() == 'pB_dchar_browse_directory':
            self.tB_dchar_directory.setText(path)
        elif sender.objectName() == 'pB_sweep_browse_directory':
            self.tB_sweep_directory.setText(path)
        else:
            self.tB_cycle_directory.setText(path)

    def on_plot_cb_check(self, state: bool):
        """
        Hide PlotWidget and resize window if plot checkbox in any tab is unchecked.
        Show PlotWidget and resize window if plot checkbox in any tab is checked.
        """
        for cb in self.plot_cb:
            cb.setChecked(state)

        if not state:
            self.plotWidget.hide()
            win_size = self.settings.value('window/size_plot_off', QSize(530, 680))
        else:
            self.plotWidget.show()
            win_size = self.settings.value('window/size_plot_on', QSize(1350, 680))
        self.setMinimumSize(win_size)
        self.resize(win_size)

    def show_warning_popup(self, text: str):
        """
        Pop up warning message box with customized text to prevent starting the measurement
        """
        warning = QMessageBox()
        warning.setText(text)
        warning.setStandardButtons(QMessageBox.Ok)
        warning.setIcon(QMessageBox.Warning)
        warning.exec()

    def set_sense(self, state):
        if state:
            self.keithley.rsense = 'ON'
        else:
            self.keithley.rsense = 'OFF'
        for cb in self.sense_cb:
            cb.setChecked(state)

    def _init_settings(self):
        settings_file = os.path.join(os.path.split(__file__)[0],
                                     'settings.ini')
        settings = QSettings(settings_file, QSettings.IniFormat)
        # file only, no fallback to register:
        settings.setFallbacksEnabled(False)
        return settings

    ##### Plot widgets
    def plot_on_data_arrive(self, sender):
        """
        Plot new data to the graph.
        //It looks that point to the data directly in worker thread instead of
        //sending them through signal is more efficient when frequency is really high.
        """
        try:
            self.curve.setData(self.worker.x, self.worker.y)
        except Exception:  # with very big load pyqtgraph raises exception with different shape of X and Y
            if hasattr(self, 'worker'):
                self.worker.signals.clear_arrays.emit()

    def _init_plot_window_chg_dchg_cycle(self, sender, source):
        """Set labels, show grid nad create curve plotter for chg/dchg/cycle tabs"""
        if source == 'V':
            ylabel = "Current (A)"
        else:
            ylabel = "Voltage (V)"
        xlabel = "Capacity (mAh)"
        self.plotWidget.getAxis('left').setLabel(ylabel)
        self.plotWidget.getAxis('bottom').setLabel(xlabel)
        self.plotWidget.showGrid(x=True, y=True)
        self.plotWidget.clear()
        self.curve = self.plotWidget.plot()

    def _init_plot_window_sweep(self):
        """Set labels, show grid nad create curve plotter for sweep tab"""
        # PlotWidget for sweep tab:
        self.plotWidget.getAxis('left').setLabel("Current (A)")
        self.plotWidget.getAxis('bottom').setLabel("Voltage (V)")
        self.plotWidget.showGrid(x=True, y=True)
        self.plotWidget.clear()
        self.curve = self.plotWidget.plot()


def except_hook(cls, exception, traceback):
    sys.__excepthook__(cls, exception, traceback)


def main():
    sys.excepthook = except_hook
    app = QApplication(sys.argv)
    win = MainWindow()
    win.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
